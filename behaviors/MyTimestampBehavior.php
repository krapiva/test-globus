<?php
/**
 * Created by PhpStorm.
 * User: krapi
 * Date: 06.05.2018
 * Time: 13:29
 */

namespace app\behaviors;


use yii\base\Behavior;
use yii\db\ActiveRecord;
use Yii;

class MyTimestampBehavior extends Behavior
{
    public $attribute;
    public $format;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'toTimestamp',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'toTimestamp',
            ActiveRecord::EVENT_AFTER_FIND => 'toDate',
            ActiveRecord::EVENT_AFTER_INSERT => 'toDate',
            ActiveRecord::EVENT_AFTER_UPDATE => 'toDate',
        ];
    }

    public function toTimestamp($event)
    {
        foreach ($this->attributeArray() as $attribute) {
            if ($this->owner->{$attribute}) {
                $this->owner->{$attribute} = Yii::$app->formatter->asTimestamp($this->owner->{$attribute});
            }
        }
    }

    public function toDate($event)
    {
        $format = $this->format ? $this->format : 'dd.MM.yyyy';
        foreach ($this->attributeArray() as $attribute) {
            if ($this->owner->{$attribute}) {
                $this->owner->{$attribute} = Yii::$app->formatter->asDate($this->owner->{$attribute}, $format);
            }
        }
    }

    public function attributeArray()
    {
        if (!$this->attribute) {
            return [];
        }
        $attributes = explode(',', $this->attribute);
        array_walk($attributes, [self::className(), 'trim_value']);
        return $attributes;
    }

    static function trim_value(&$value)
    {
        $value = trim($value);
    }
}