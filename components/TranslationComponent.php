<?php
/**
 * Created by PhpStorm.
 * User: krapi
 * Date: 12.05.2018
 * Time: 21:15
 */

namespace app\components;


use yii\base\Component;

class TranslationComponent extends Component
{
    public $classList = [];

    protected $classesList = [];
    protected $namesList = [];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->classesList = $this->setClassesList();
        $this->namesList = $this->setNamesList();
    }


    protected function setClassesList()
    {
        $result = [];
        foreach ($this->classList as $class) {
            $result[$class::tableName()] = $class;
        }
        return $result;
    }

    public function getClassesList()
    {
        return $this->classesList;
    }

    protected function setNamesList()
    {
        $result = [];
        foreach ($this->classList as $class) {
            $result[$class::tableName()] = $class::tableLabel();
        }
        return $result;
    }

    public function getNamesList()
    {
        return $this->namesList;
    }

    public function getClassForTableName($tableName)
    {
        return isset($this->classesList[$tableName]) ? $this->classesList[$tableName] : null;
    }

    public function getLabelForTableName($tableName)
    {
        return isset($this->namesList[$tableName]) ? $this->namesList[$tableName] : null;
    }
}