<?php
/**
 * Created by PhpStorm.
 * User: krapi
 * Date: 12.05.2018
 * Time: 21:34
 */

namespace app\controllers;


use app\models\Translation;
use yii\web\Controller;
use Yii;

class TranslationController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index', [
            'namesList' => Yii::$app->translation->getNamesList()
        ]);
    }

    public function actionList($id)
    {
        $model = new Translation($id);

        if (Yii::$app->request->post('items')) {
           $model->loadItems(Yii::$app->request->post('items'));
           if ($model->save()) {
               return $this->redirect('index');
           }
        }

        return $this->render('list', [
            'items' => $model->items,
            'tableName' => $id
        ]);
    }

}