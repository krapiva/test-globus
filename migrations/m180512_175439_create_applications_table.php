<?php

use yii\db\Migration;

/**
 * Handles the creation of table `applications`.
 */
class m180512_175439_create_applications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('applications', [
            'id' => $this->primaryKey(),
            'firstName' => $this->string(),
            'secondName' => $this->string(),
            'patronymic' => $this->string(),
            'email' => $this->string(),
            'doctorTypeId' => $this->integer(),
            'scienceDegreeId' => $this->integer(),
            'description' => $this->text(),
            'date' => $this->integer(),
            'paid' => $this->boolean(),
            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
        ]);

        $this->createTable('doctorTypes', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);

        $this->createTable('scienceDegrees', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);

        $this->createTable('translations', [
            'id' => $this->primaryKey(),
            'table' => $this->string(),
            'rowId' => $this->integer(),
            'title' => $this->string()
        ]);

        $this->addForeignKey('fk-applications-doctorTypeId', 'applications', 'doctorTypeId', 'doctorTypes', 'id', 'CASCADE');
        $this->addForeignKey('fk-applications-scienceDegreeId', 'applications', 'scienceDegreeId', 'scienceDegrees', 'id', 'CASCADE');

        Yii::$app->db->createCommand()
            ->batchInsert('doctorTypes', ['title'],[
                ['Tерапевт'],
                ['Xирург'],
                ['Педиатр'],
            ])
            ->execute();

        Yii::$app->db->createCommand()
            ->batchInsert('scienceDegrees', ['title'],[
                ['Специалист'],
                ['Кандидат наук'],
                ['Доктор наук'],
            ])
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-applications-doctorTypeId', 'applications');
        $this->dropForeignKey('fk-applications-scienceDegreeId', 'applications');
        $this->dropTable('applications');
        $this->dropTable('doctorTypes');
        $this->dropTable('scienceDegrees');
        $this->dropTable('translations');
    }
}
