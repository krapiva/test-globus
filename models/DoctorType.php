<?php

namespace app\models;

use app\models\interfaces\ListInterface;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "doctorTypes".
 *
 * @property int $id
 * @property string $title
 *
 * @property Application[] $applications
 */
class DoctorType extends \yii\db\ActiveRecord implements interfaces\ListInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctorTypes';
    }

    /**
     * @return string
     */
    public static function tableLabel()
    {
        return 'Специализация доктора';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['doctorTypeId' => 'id']);
    }

    public static function asArray($lang=null)
    {
        if ($lang == 'en') {
            $model = new Translation(self::tableName());
            return $model->getTranslateListForTable();
        }
        return ArrayHelper::map(self::find()->all(), 'id', 'title');
    }
}
