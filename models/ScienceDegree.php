<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "scienceDegrees".
 *
 * @property int $id
 * @property string $title
 *
 * @property Applications[] $applications
 */
class ScienceDegree extends \yii\db\ActiveRecord implements interfaces\ListInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'scienceDegrees';
    }

    /**
     * @return string
     */
    public static function tableLabel()
    {
        return 'Квалификация доктора';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['scienceDegreeId' => 'id']);
    }

    public static function asArray($lang=null)
    {
        if ($lang == 'en') {
            $model = new Translation(self::tableName());
            return $model->getTranslateListForTable();
        }
        return ArrayHelper::map(self::find()->all(), 'id', 'title');
    }
}
