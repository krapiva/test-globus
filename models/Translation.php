<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "translations".
 *
 * @property int $id
 * @property string $table
 * @property int $rowId
 * @property string $title
 */
class Translation extends \yii\db\ActiveRecord
{
    protected $items = [];
    protected $tableName;
    protected $rows;
    protected $listItems;

    public function __construct($tableName, array $config = [])
    {
        $this->tableName = $tableName;
        $this->setParams();

        parent::__construct($config);
    }

    protected function setParams()
    {
        $this->setListItems();
        $this->setItems();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'translations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rowId'], 'default', 'value' => null],
            [['rowId'], 'integer'],
            [['table', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table' => 'Table',
            'rowId' => 'Row ID',
            'title' => 'Title',
        ];
    }

    /**
     * загружаем поля для актив рекорд класса
     * @return array
     */
    protected function setListItems()
    {
        $className = Yii::$app->translation->getClassForTableName($this->tableName);
        $result = $className::asArray();
        $this->listItems = $result;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     * получаем записи переводов таблицы
     */
    protected function getTranslationForTable()
    {
        $rows = (new \yii\db\Query())
            ->select('*')
            ->from(self::tableName())
            ->where(['table' => $this->tableName])
            ->all();
        return ArrayHelper::map($rows, 'rowId', 'title');
    }

    /**
     * загружаем параметры в items через конструктор
     */
    protected function setItems()
    {

        $translation = $this->getTranslationForTable();
        $result = [];
        foreach ($this->listItems as $id => $label) {
            $result[$id]['label'] = $label;
            $result[$id]['translate'] = isset($translation[$id]) ? $translation[$id] : null;
        }
        $this->items = $result;
    }

    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param $data
     * @return bool
     * загружаем параметры для обновления переводов
     */
    public function loadItems($data)
    {
        $result = [];
        foreach ($data as $key => $value) {
            if ($key && isset($this->listItems[$key])) {
                $result[] = [
                    'table' => $this->tableName,
                    'rowId' => $key,
                    'title' => $value,
                ];
            }
        }
        $this->rows = $result;
        return true;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * сохраняем переводы
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        self::deleteAll(['table' => $this->tableName]);
        if (count($this->rows) > 0) {
            Yii::$app->db->createCommand()
                ->batchInsert(self::tableName(), ['table', 'rowId', 'title'], $this->rows)
                ->execute();
        }
        return true;
    }

    public function getTranslateListForTable()
    {
        return $this->getTranslationForTable();
    }
}
