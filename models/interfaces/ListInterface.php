<?php
/**
 * Created by PhpStorm.
 * User: krapi
 * Date: 12.05.2018
 * Time: 22:06
 */

namespace app\models\interfaces;


interface ListInterface
{
    /**
     * @return mixed
     * название таблицы типа список
     */
    public static function tableLabel();

    /**
     * @return mixed
     * название таблицы в бд
     */
    public static function tableName();

    /**
     * @param null $lang
     * @return mixed
     * массив из списка типа $id => $title
     */
    public static function asArray($lang=null);
}