<?php

namespace app\modules\api\controllers;

use app\models\Application;
use app\models\DoctorType;
use app\models\ScienceDegree;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter' => [
                'class' => VerbFilter::className(),
                'actions' => $this->verbs(),
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return array
     * добавляем новую заявку
     */
    public function actionCreateApplication()
    {
        $model = new Application();
        $model->firstName = Yii::$app->request->post('firstName');
        $model->secondName = Yii::$app->request->post('secondName');
        $model->patronymic = Yii::$app->request->post('patronymic');
        $model->description = Yii::$app->request->post('description');
        $model->date = Yii::$app->request->post('date');
        $model->doctorTypeId = Yii::$app->request->post('doctorTypeId');
        $model->scienceDegreeId = Yii::$app->request->post('scienceDegreeId');
        $model->email = Yii::$app->request->post('email');
        if ($model->save()) {
            return ['status' => 'success'];
        } else {
            return ['status' => 'error', 'message' => $model->errors];
        }
    }

    public function actionDoctorType($lang=null)
    {
        return ['doctorType' => DoctorType::asArray($lang)];
    }

    public function actionScienceDegree($lang=null)
    {
        return ['scienceDegree' => ScienceDegree::asArray($lang)];
    }
}
