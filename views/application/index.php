<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(['id' => 'application-pjax']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{checkPaid}',
                'buttons' => [
                    'checkPaid' => function ($url, $model) {

                        return Html::checkbox('', $model->paid, [
                                'onclick' => '
                                    if ($(this).attr("checked")) {
                                        var paid = 0;
                                    } else {
                                        var paid = 1;
                                    }
                                    $.get("' . Url::to(['check-paid']) . '",{"id":"' . $model->id . '","paid":paid},function(data){
                                        console.log(data);
                                    });
                                '
                        ]);
                    }

                ]
            ],
            [
                'attribute' => 'id',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a($data->id, ['update', 'id'=>$data->id]);
                }
            ],
            'firstName',
            'secondName',
            'patronymic',
            'email:email',

        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
