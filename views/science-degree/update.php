<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ScienceDegree */

$this->title = 'Изменить квалификацию: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Квалификации докторов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="science-degree-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
