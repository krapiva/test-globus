<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Списки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doctor-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php foreach ($namesList as $key => $value) : ?>
        <div>
            <?= Html::a($value, ['list', 'id' => $key]) ?>
        </div>
    <?php endforeach; ?>

</div>