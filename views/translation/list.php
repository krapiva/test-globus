<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Переводы списка ' . Yii::$app->translation->getLabelForTableName($tableName);
$this->params['breadcrumbs'][] =  ['label' => 'Списки', 'url' => ['index']];;
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php ActiveForm::begin() ?>
    <?php foreach ($items as $key => $value) : ?>
        <div class="form-group">
            <?= Html::label($value['label']) ?>
            <?= Html::input('string', 'items['. $key .']', $value['translate'], ['class' => 'form-control']) ?>
        </div>
    <?php endforeach; ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end() ?>

</div>